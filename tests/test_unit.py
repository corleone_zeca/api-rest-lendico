from flask import Flask
import unittest
import json

from consulta import consulta_view

__author__ = 'Jose Alvaro Sacramento <jsacramento22@hotmail.com>'

app = Flask(__name__)


class TestUnit(unittest.TestCase):
    """Testes unitarios para a classe XXX
    ___________________________________

    Para execucao dos testes unitarios:

    $ docker exec -i -t <container_id> /bin/bash
    $ python3 -m unittest discover -s tests
    """

    def test_regular(self):
        """
        Test para consultas com CPF status "regular"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('40442820135')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'regular'})

    def test_falecido(self):
        """
        Test para consultas com CPF status "falecido"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('05137518743')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'falecido'})

    def test_suspenso(self):
        """
        Test para consultas com CPF status "suspenso"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('40532176871')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'suspenso'})

    def test_irregular(self):
        """
        Test para consultas com CPF status "irregular"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('07691852312')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'irregular'})

    def test_cancelado(self):
        """
        Test para consultas com CPF status "cancelado"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('01648527949')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'cancelado'})

    def test_nulo(self):
        """
        Test para consultas com CPF status "nulo"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('98302514705')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'nulo'})

    def test_cancelado_oficio(self):
        """
        Test para consultas com CPF status "cancelado"
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('64913872591')
            resp = json.loads(response.data.decode('utf8').replace("'", '"'))

            self.assertEqual(response.status_code, 200)
            self.assertEqual(resp, {'status': 'cancelado'})

    def test_cpf_irregular(self):
        """
        Test para consultas com CPF inválido, ou nao constam na base da SERPRO
        """
        with app.app_context():
            response = consulta_view.ConsultaView().get_consulta('4044282013')
            resp = json.loads(
                response[0].data.decode('utf8').replace("'", '"'))

            self.assertEqual(response[1], 400)
            self.assertEqual(resp, {'error': {'reason': 'CPF inv\xe1lido'}})


if __name__ == '__main__':
    unittest.main()
