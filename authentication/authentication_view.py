
__author__ = 'Jose Alvaro Sacramento <jsacramento22@hotmail.com>'


class Authentication():

    def __init__(self, **kwargs):
        """Para fins de exercicio, os tokens foram salvos no código,
        em uma aplicacao esses deveriam estar em um Banco de dados
        """
        self.tokens = {
            "secret-token-1": "jose",
            "secret-token-2": "maria"
        }

    def get_Authentication(self, token):
        """ Confirma se os tokens sao validos

        Args:
            token (str): the token from the request.header
        """
        if token in self.tokens:
            return True
        return False
