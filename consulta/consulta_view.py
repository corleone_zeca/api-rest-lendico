from flask import jsonify, Flask

import requests
import os
import json

__author__ = 'Jose Alvaro Sacramento <jsacramento22@hotmail.com>'

app = Flask(__name__)


class ConsultaView():
    def get_consulta(self, cpf):
        """ Consulta situacao de CPFs na base de dados da SERPRO

            Somente para CPFs cadastrados na base da SERPRO

        Args:
            cpf (int): the integer from the request
        """
        try:
            # Buscar as variaveis de ambiente
            auth_token = os.environ.get('SERPRO_TOKEN')
            serpro_url = os.environ.get('SERPRO_URL')

            """ A descricao retornada pela SERPRO eh dif da esperada,
            por isso o tratamento pelo codigo"""
            status = {
                '0': 'regular',
                '2': 'suspenso',
                '3': 'falecido',
                '4': 'irregular',
                '5': 'cancelado',
                '8': 'nulo',
                '9': 'cancelado'
            }

            # Para executar a consulta na SERPRO é necessio autenticacao
            hed = {'Authorization': 'Bearer {}'.format(auth_token)}
            url = '{}{}'.format(serpro_url, cpf)

            # Chamada para a SERPRO
            response = requests.get(url, headers=hed)

            # Formatando a resposta foi em bytes
            resp_desc = response.content.decode('utf8').replace("'", '"')

            """ Caso retorno de erro da SERPRO,
             formata e repassa a msg e o status code """
            if response.status_code != 200:
                app.logger.error(
                    'Retorno API SERPRO: Status code: {},'
                    'Raw Descricao: {}'.format(
                        response.status_code, resp_desc))
                return jsonify(
                    {'error': {
                        'reason': resp_desc}}), response.status_code

            # Gerando os logs
            app.logger.info('CPF consultado: {}'.format(cpf))
            app.logger.info(
                'Retorno API SERPRO: Status code: {} Raw Descricao: {}'.format(
                    response.status_code, resp_desc))

            # Formatando a resposta, caso sucesso
            resp_json = json.loads(resp_desc)
            ret_obj = {'status': status[resp_json['situacao']['codigo']]}
            return jsonify(ret_obj)

        except Exception as e:
            # Erro 500 em caso de falha do processo
            app.logger.error(e)
            return jsonify({'error': {'reason': e}}), 500
