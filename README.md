# LENDICO - API Rest
Teste para desenvolvedor Back-end

### 1 - Tecnologias utilizadas
* [Python](https://www.pyhon.org/)- v3.5.2 - Linguagem principal.
* [Docker](https://www.docker.com/) v2.1 - Criação e gerenciamento de ambiente.
* [Docker-compose](https://docs.docker.com/compose/install/) v1.24 - Scripts para simplificar a execução do Docker.
* [Flask](https://pypi.org/project/Flask/) v1.0.3 - Framework de desenvolvimento.
* [Requests](https://pypi.org/project/requests/) v2.9.1 - Extenção para realizar as chamadas REST.
* [Unittest](https://docs.python.org/3.5/library/unittest.html) v3.5.2 - Framework para execução dos testes unitários. Embarcado no pacote do Python.
* [Flask-httpauth](https://flask-httpauth.readthedocs.io/en/latest/) v3.3.0 - Extenção para gerenciamento da Autenticação HTTP no Flask.

### 2 - Estrutura de diretórios
```bash
.
├── .env
├── .gitignore
├── Dockerfile
├── README.md
├── app.py
├── authentication
│   └── authentication_view.py
├── consulta
│   └── consulta_view.py
├── docker-compose.yml
├── postman_collection.json
├── requirements.txt
└── tests
    └── test_unit.py
```


### 3 - Pré-requisitos para instalação

Necessário [Docker](https://www.docker.com/) v2.1+ e [Docker-compose](https://docs.docker.com/compose/install/) v1.24+ instalados na máquina para rodar. Para usuários de Mac, o Docker-compose já é instalado junto com o Docker, não necesitando instalação a parte.

### 4 - Instalando a aplicação:

```sh
$ cd <diretorio-raiz-do-projeto>
$ docker-compose build api
```

### 5 - Executando a aplicação:

```sh
$ cd <diretorio-raiz-do-projeto>
$ docker-compose up api
```

### 6 - Realizando chamadas na aplicação:

Poderá ser utilizado um Client de Rest, exemplo [Postman](https://www.getpostman.com). Existe na raís do repositório um arquivo `postman_collection.json` com uma collection de exemplos de chamadas. A aplicação esta configurada para responder em `localhost:5000`. Na collection do Postman existe uma chamada que pode ser executada para **Verificar Status da API**.

### 7 - Execução dos Testes Unitários:
Para execução dos testes unitários, deve estar previamente rodando em uma janela do terminal (ítem 5). Em outra janela do terminal executar os comandos:

```sh
$ cd <diretorio-raiz-do-projeto>
$ docker ps <pegar o container_id do container>
$ docker exec -i -t <container_id> /bin/bash
root@container:/app# python3 -m unittest discover -s tests
```

### 7 - Dúvidas:
jsacramento22@hotmail.com