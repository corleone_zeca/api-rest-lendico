from flask import Flask, jsonify
from runenv import load_env

from consulta import consulta_view
from authentication import authentication_view
import os

from flask_httpauth import HTTPTokenAuth

__author__ = 'Jose Alvaro Sacramento <jsacramento22@hotmail.com>'

app = Flask(__name__)
load_env()

auth = HTTPTokenAuth(scheme='Token')


@app.route('/')
def index():
    # Para verificacao do status da api
    environ = os.environ.get('ENV')
    resp_dict = {'environment': environ, 'status': 'running'}
    response = jsonify(resp_dict)
    return response


@app.route('/api/v1.0/consulta/<int:cpf>', methods=['GET'])
@auth.login_required
def consulta(cpf):
    return consulta_view.ConsultaView().get_consulta(cpf)


@auth.verify_token
def verify_token(token):
    return authentication_view.Authentication().get_Authentication(token)


# include this for local dev
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
